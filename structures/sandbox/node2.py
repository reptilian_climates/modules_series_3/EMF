

'''
	priorities
		
		start 1 node
			flask & rethink
		
		start 2 nodes
			flask & rethink
'''


def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'modules',
	'modules_pip'
])

print ('node')


import EMF.node as EMF_node
EMF_node.start ({
	"flask": {
		"port": "40027"
	},
	"rethink": {
		"ports": {
			"driver": "40028",
			"cluster": "40029",
			"http": "40030"
		} 
	},
	"trustees": [
		"localhost:40026"			
	]
})

