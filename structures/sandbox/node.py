

'''
	priorities
		
		start 1 node
			flask & rethink
		
		start 2 nodes
			flask & rethink
			
		start network nodes (rethink is only internally accessible):
			000.00.00.000:443
			000.00.00.001:443
'''


def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../modules',
	'../modules_pip'
])

print ('node')

ports = [ 40023, 40024, 40025, 40026 ]

import EMF.node as EMF_node
EMF_node.start ({
	"flask": {
		"port": "40023"
	},
	"rethink": {
		"ports": {
			"driver": "40024",
			"cluster": "40025",
			"http": "40026"
		} 
	},
	"trustees": [
		"localhost:40027"			
	]
})
