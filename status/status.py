


def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'structures_pip'
])

import sys
print (sys.argv)



import pathlib
from os.path import dirname, join, normpath
this_folder = pathlib.Path (__file__).parent.resolve ()
structures = normpath (join (this_folder, "../structures"))

guarantees = str (normpath (join (this_folder, "guarantees")))

if (len (sys.argv) >= 2):
	glob_string = guarantees + '/' + sys.argv [1]
else:
	glob_string = guarantees + '/**/status_*.py'

print ("glob string:", glob_string)

import vivacious
scan = vivacious.start (
	glob_string = glob_string,
	
	simultaneous = True,
	
	module_paths = [
		normpath (join (structures, "modules")),
		normpath (join (structures, "modules_pip"))
	],
	
	relative_path = guarantees
)



#
#
#